//
//  ViewController.swift
//  InstaRate
//
//  Created by Indieg0 on 24.06.16.
//  Copyright © 2016 Kirill. All rights reserved.
//

import UIKit
import Foundation


class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var token : String = ""
    let fullURL = "https://api.instagram.com/oauth/authorize/?client_id=fc82a639891748dea4d792c9d3991a01&redirect_uri=http://localhost&response_type=token"
        override func viewDidLoad() {
        super.viewDidLoad()
            
            self.loadURL()
  
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadURL() {
        let url = NSURL(string:fullURL)
        let urlRequest = NSURLRequest(URL: url!)
        webView.loadRequest(urlRequest)
      //  webView.delegate = self
        
        
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
       var urlString = request.URL?.absoluteString
        print("URL STRING: \(urlString)")
        let urlParts = urlString?.componentsSeparatedByString("http://localhost/")
        
        if urlParts?.count > 1 {
            urlString = urlParts![1]
            let accessToken = urlString?.rangeOfString("#access_token=")
            
            let stringAccessToken = urlString?.substringFromIndex(accessToken!.endIndex)
            print("Access Token = \(stringAccessToken)")
            
          NSUserDefaults.standardUserDefaults().setObject(stringAccessToken, forKey: "token")
            
            token = stringAccessToken!
            self.getAvatarAndFollowers()
            
        }
        
        
        return true
    }
    
    func getAvatarAndFollowers() {
        let string = "https://api.instagram.com/v1/users/self/?access_token=\(token)"
        let data = NSData(contentsOfURL: NSURL(string:  string)!)
        
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            
            let follows = json.valueForKeyPath("data.counts.follows")
            let followed = json.valueForKeyPath("data.counts.followed_by")
            let posts = json.valueForKeyPath("data.counts.media")
            let username = json.valueForKeyPath("data.username")
            let avatar = json.valueForKeyPath("data.profile_picture")

            
            let storyboard = self.storyboard
            let vc = storyboard!.instantiateViewControllerWithIdentifier("mainViewController") as! MainTableViewController
            
            vc.followed = String(format:"%d", followed!.integerValue)
            vc.follows = String(format:"%d", follows!.integerValue)
            vc.posts = String(format:"%d", posts!.integerValue)
            vc.username = username as! String
            vc.avatar = avatar as! String
            
            self.presentViewController(vc, animated: true, completion: nil)
           

            
            print("Follows: \(follows), Followed By:\(followed), Posts:\(posts), Username:\(username), PICURL:\(avatar)")
            
   
            
        } catch {
            print(error)
        }
    }

}



